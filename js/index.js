const nombre = $(".poke_nombre");
const id = $(".poke_id");
const peso = $(".poke_peso");
const tipo = $(".poke_tipo");
const movimientos = $(".poke_movimientos");
const img = $(".poke_img");

const typeColors = {
    electric: '#FFEA70',
    normal: '#B09398',
    fire: '#FF675C',
    water: '#0596C7',
    ice: '#AFEAFD',
    rock: '#999799',
    flying: '#7AE7C7',
    grass: '#4A9681',
    psychic: '#FFC6D9',
    ghost: '#561D25',
    bug: '#A2FAA3',
    poison: '#795663',
    ground: '#D2B074',
    dragon: '#DA627D',
    steel: '#1D8A99',
    fighting: '#2F2F2F',
    default: '#2A1A1F',
};



$("#buscar").click(function(){
	event.preventDefault();
var poke = $("#select_poke").val();
console.log(poke);
	fetch(`https://pokeapi.co/api/v2/pokemon/${poke.toLowerCase()}`)
		.then(data => data.json())
		.then(response => poke_info(response))
		.catch(err => no_existe())
})

const poke_info = data => {
    limpiar();


	const colorOne = typeColors[data.types[0].type.name];
    const colorTwo = data.types[1] ? typeColors[data.types[1].type.name] : typeColors.default;
    img.css( "background", `radial-gradient(${colorTwo} 33%, ${colorOne} 33%)` );
    img.css( "backgroundSize", '5px 5px' );
    // img.style.backgroundSize = ' 5px 5px';


	nombre.append("<h2>" + data.name + "</h2>");
	id.append("<strong>NOMERO EN LA POKEDEX: </strong> " + data.id);
	peso.append("<strong>PESO: </strong>" + data.weight + "KG");
	tipo.append("<div style='color: "+ typeColors[data.types[0].type.name] +";'>" + JSON.stringify(data.types[0].type.name).toUpperCase() + "</div>");
	movimientos.append("<strong>MOVIMIENTOS: </strong>" + data.moves.length);
	var imgs = '<div id="carouselExampleControls" class="carousel slide" data-bs-ride="carousel">'+
	'<div class="carousel-inner">'+
	  '<div class="carousel-item active">'+
		'<img src="'+ data.sprites.front_default +'" class="d-block w-100" alt="...">'+
	  '</div>'+
	  '<div class="carousel-item">'+
		'<img src="'+ data.sprites.front_shiny +'" class="d-block w-100" alt="...">'+
	  '</div>'+
	'</div>'+
	'<button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="prev">'+
	  '<span class="carousel-control-prev-icon" aria-hidden="true"></span>'+
	  '<span class="visually-hidden">Previous</span>'+
	'</button>'+
	'<button class="carousel-control-next" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="next">'+
	  '<span class="carousel-control-next-icon" aria-hidden="true"></span>'+
	  '<span class="visually-hidden">Next</span>'+
	'</button>'+
  '</div>'

	img.append(imgs);
	$("#select_poke").val('');
}

const no_existe = () => {
	limpiar();
	var quien = "https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/f82cc357-e354-4ef7-8b2d-647f6f756800/dbf1jrd-095f7fd1-e33b-4e26-b456-8cbf40d0e5d1.png/v1/fill/w_1024,h_765,q_80,strp/quien_es_ese_pokemon__who_s_that_poke___by_shikomt_by_shikomt_dbf1jrd-fullview.jpg?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwiaXNzIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsIm9iaiI6W1t7ImhlaWdodCI6Ijw9NzY1IiwicGF0aCI6IlwvZlwvZjgyY2MzNTctZTM1NC00ZWY3LThiMmQtNjQ3ZjZmNzU2ODAwXC9kYmYxanJkLTA5NWY3ZmQxLWUzM2ItNGUyNi1iNDU2LThjYmY0MGQwZTVkMS5wbmciLCJ3aWR0aCI6Ijw9MTAyNCJ9XV0sImF1ZCI6WyJ1cm46c2VydmljZTppbWFnZS5vcGVyYXRpb25zIl19.YZOIomNz5t-pjv59EuK-mtru0QgjhlTtGEGPLuzR1hM";
	nombre.append("<h2> No hay datos</h2>");
	id.append("<strong>NOMERO EN LA POKEDEX:</strong>  No hay datos");
	peso.append("<strong>PESO: </strong> No hay datos");
	tipo.append("<div'>No hay datos</div>");
	movimientos.append("<strong>MOVIMIENTOS: </strong>No hay datos");
	var imgs = '<div id="carouselExampleControls" class="carousel slide" data-bs-ride="carousel">'+
	'<div class="carousel-inner">'+
	  '<div class="carousel-item active">'+
		'<img src="'+ quien+'" class="d-block w-100" alt="...">'+
	  '</div>'+
	  '<div class="carousel-item">'+
		'<img src="'+ quien +'" class="d-block w-100" alt="...">'+
	  '</div>'+
	'</div>'+
	'<button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="prev">'+
	  '<span class="carousel-control-prev-icon" aria-hidden="true"></span>'+
	  '<span class="visually-hidden">Previous</span>'+
	'</button>'+
	'<button class="carousel-control-next" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="next">'+
	  '<span class="carousel-control-next-icon" aria-hidden="true"></span>'+
	  '<span class="visually-hidden">Next</span>'+
	'</button>'+
  '</div>'

	img.append(imgs);
	$("#select_poke").val('');
}

$("#limpiar").click(function(){
	limpiar();
})
function limpiar(){
	nombre.empty();
	id.empty();
	peso.empty();
	tipo.empty();
	movimientos.empty();
	img.empty();
}